import pandas as pd
import numpy as np
import random
from sklearn.model_selection import train_test_split

# reads a .csv into a pandas dataframe
def get_data(fname, manip=None):
    assert '.csv' == fname[-4:], 'File must be a .CSV'
    df = pd.read_csv(fname)
    if manip == None:
        return df
    return manip(df)

# general data cleaning
def clean_data(df, tgt_name=None, numeric_only=False,
        ignore=[], drop_nan=True, rep_nan=True, dummify=[], bin=[]):
    # remove unwanted columns
    for col in ignore:
        if col in df.columns:
            del df[col]
    # find most common value in each column
    if rep_nan:
        orig_len = len(df)
        df = df.fillna(df.mean())
    # drop rows with NAN
    if drop_nan:
        df = df.dropna(thresh=len(df.columns))
    if rep_nan:
        assert len(df) == orig_len, 'Drop NAN removed values'
    # create dummy variables
    for col in dummify:
        if col in df.columns:
            for val in list(set(df[col]))[:-1]:
                df[col+'_'+val] = (df[col] == val).astype(int)
    # bin continuous variables
    for bin_pair in bin:
        if bin_pair[0] in df.columns:
            df[bin_pair[0]] = (df[bin_pair[0]] / bin_pair[1]).astype(int)
    # remove non-numeric data
    if numeric_only:
        df = df.select_dtypes(include=[np.number])
    # get target dataframe
    tgt = None
    if tgt_name != None:
        assert tgt_name in df.columns, 'Invalid target column name: ' + str(tgt_name)
        tgt = df[tgt_name]
        del df[tgt_name]
    return df, tgt

# creates randomized training and test sets from the dataframe
def split_data(df, tgt, test_perc=0.4):
    return train_test_split(
        df, tgt, test_size=test_perc)

    # find indices for training & test sets
    split = int(len(tgt) * train_perc)
    ndxs = list(range(len(tgt)))
    random.shuffle(ndxs)
    train_ndxs = ndxs[:split]
    test_ndxs = ndxs[split:]
    # get data
    get = lambda d, ixs: np.array([d.iloc(i) for i in ixs])
    train_data = get(df, train_ndxs)
    test_data = get(df, test_ndxs)
    train_tgt = get(tgt, train_ndxs)
    test_tgt = get(tgt, test_ndxs)
    return train_data, test_data, train_tgt, test_tgt