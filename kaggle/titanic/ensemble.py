import numpy as np
import pandas as pd
import ml_helper as helper

# performs voting across multiple classifiers
def vote(classifiers, data):
    # do predictions
    predictions = {cl: cl.predict(data) for cl in classifiers}
    # vote
    return pd.DataFrame(data=predictions).mode(axis=1)[0].astype(int)

# bag ensemble technique
def bag(train_data, train_tgt, test_data, newClassifier, bag_perc=0.6,
        num_classifiers=5):
    # create classifiers
    cls = [newClassifier() for i in range(num_classifiers)]
    # find data indices for each bag
    bag_ndxs = [np.random.binomial(1, bag_perc, size=len(train_data)).astype(bool)
        for i in range(num_classifiers)]
    # train classifiers
    for i in range(num_classifiers):
        cls[i].fit(train_data[bag_ndxs[i]], train_tgt[bag_ndxs[i]])
    # predict on test data
    return vote(cls, test_data)