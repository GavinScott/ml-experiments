import pandas as pd
import ensemble 

# calculates the percent accuracy of the predictions
def accuracy(predicted, truth):
    assert len(predicted) == len(truth), \
        'Predicted and truth values mut have the same length'
    correct = sum([1 for i in range(len(truth)) if truth[i] == predicted[i]])
    return correct / len(truth)

# fits a list of classifiers to the given data and target
def group_fit(data, tgt, *classifiers):
    for cl in classifiers:
        cl[1].fit(data, tgt)
    return classifiers

# outputs the accuracy of each classifier in the given list
# when trained and tested on the given data
def compare_classifiers(train_data, train_tgt, test_data, test_tgt,
        *classifiers):
    test_target = test_tgt.as_matrix()
    classifiers = group_fit(train_data, train_tgt, *classifiers)
    acc_sum = 0
    for cl in classifiers:
        acc = accuracy(cl[1].predict(test_data), test_target)
        print('Acc', cl[0], ':', acc)
        acc_sum += acc
    print('\nAverage accuracy:', acc_sum / len(classifiers))
    return [cl[1] for cl in classifiers]

# combines dataframes horizontally
def combine_dataframes(*dfs):
    # assert valid merge
    for i in range(1, len(dfs)):
        assert len(dfs[i - 1][1]) == len(dfs[i][1]), \
            'Dataframes must have same lengths'
    # init data
    d = {}
    for df in dfs:
        for col in df[0]:
            d[col] = []
    # merge
    for i in range(len(dfs[0][1])):
        for df in dfs:
            for col in df[0]:
                d[col].append(df[1].iloc[i])
    return pd.DataFrame(data=d)

def output_predictions(predicted, ids, id_name, class_name, fname):
    output = combine_dataframes(
        (['PassengerId'], ids),
        (['Survived'], predicted))
    
    output.to_csv(fname, index=False)