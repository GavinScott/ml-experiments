import pandas as pd

def get_col_value_counts(df, column):
    count = {}
    for v in df[column]:
        if v not in count:
            count[v] = 1
        else:
            count[v] += 1
    return count

# prints out the unique values for each column along with their count
def value_distribution(df, max_vals=5):
    if isinstance(df, pd.Series):
        df = df.to_frame()
    for col in df.columns:
        count = {}
        for v in df[col]:
            if v not in count:
                count[v] = 1
            else:
                count[v] += 1
        print(col)
        for key in count:
            print('\t', key, ':', count[key])

