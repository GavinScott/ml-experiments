import numpy as np
import pandas as pd
import data_extract as de
import ml_helper as helper
import ensemble
import data_visualization as vis
# classifiers
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import RandomForestClassifier

# modifying columns in data
def manipulation(df):
    family_size = df['Parch'] + df['SibSp']
    # column for large families
    df['LargeFamily'] = family_size.apply(
        lambda x: 1 if x > 3 else 0)
    # column for single-riders
    df['Alone'] = family_size.apply(
        lambda x: int(x == 0))
    # column for total size of family
    df['FamilySize'] = family_size
    return df

# data manipulation parameters
ign_test = ['Name', 'Ticket', 'Cabin', 'Embarked', 'Parch', 'SibSp', 'Fare']
ign_train = ign_test + ['PassengerId']
bin = [('Fare', 30), ('Age', 15)]
dummify=['Sex']

# get training data
df = de.get_data('train.csv', manip=manipulation)
# general data cleaning
df, tgt = de.clean_data(df, 'Survived', numeric_only=True,
    ignore=ign_train, dummify=dummify, bin=bin)
vis.value_distribution(df)

# run trials
num_trials = 30
acc_sum = 0
print()
for i in range(num_trials):
    print('Trial:', i)
    # split data into training and test sets
    train_data, test_data, train_tgt, test_tgt = de.split_data(df, tgt)

    # compare performance of different classifiers
    # helper.compare_classifiers(train_data, train_tgt, test_data, test_tgt,
    #     ('DT ', DecisionTreeClassifier()),
    #     ('SVM', SVC()),
    #     ('NB ', GaussianNB()),
    #     ('KNN', KNeighborsClassifier()),
    #     ('RF ', RandomForestClassifier(n_estimators=len(df.columns), max_features=1)),
    #     ('ADA', AdaBoostClassifier())
    # )

    # bagging parameters
    num_classifiers = 10
    bag_perc = 0.7

    # test bagging (with SVM)
    p = ensemble.bag(train_data, train_tgt, test_data, DecisionTreeClassifier,
        num_classifiers=num_classifiers, bag_perc=bag_perc)
    acc_sum += helper.accuracy(p, test_tgt.as_matrix())

print('\nBagging Test:', acc_sum / num_trials)

# get test data
df_test = de.get_data('test.csv', manip=manipulation)
df_test, df_tgt = de.clean_data(df_test, None, numeric_only=True,
    ignore=ign_test, dummify=dummify, bin=bin)
# remove and save ids (for building predictions file)
ids = df_test['PassengerId']
del df_test['PassengerId']

# run bagging on test data
p_test = ensemble.bag(df, tgt, df_test, DecisionTreeClassifier,
    num_classifiers=num_classifiers, bag_perc=bag_perc)
assert len(p_test) == len(df_test), 'Invalid output length'

# save predictions to a file
helper.output_predictions(p_test, ids, id_name='PassengerId',
    class_name='Survived', fname='predicted.csv')